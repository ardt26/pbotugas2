import Level1.Lingkaran;
import Level2.*;
import Level3.Juring3D;
import Level3.KerucutTerpancung;
import Level3.Tembereng3D;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String lagi;
        boolean exit = false;
        Scanner scanner = new Scanner(System.in);
        do{
            try {
                System.out.println("----- Menghitung Bagian Dari Lingkaran -----");
                System.out.println("Harap masukkan bilangan positif!");
                System.out.print("Masukkan Jari-Jari : ");
                double dJariJari = scanner.nextDouble();
                System.out.print("Masukkan Tinggi : ");
                double dTinggi = scanner.nextDouble();
                System.out.print("Masukkan Sudut : ");
                double dSudut = scanner.nextDouble();
                System.out.print("Masukkan Tinggi Kerucut yang ingin dipotong : ");
                double dTinggiTerpancung = scanner.nextDouble();
                System.out.print("Masukkan Tinggi Tembereng Pada Bola : ");
                double dTinggiTembereng = scanner.nextDouble();

                if (dJariJari < 0 || dTinggi < 0  || dTinggiTerpancung < 0) {
                    throw new IllegalArgumentException("Tolong Hanya Memasukkan Bilangan Positif");
                }

                if (dTinggiTerpancung > dTinggi){
                    throw new IllegalArgumentException("Tinggi Kerucut Terpancung Harus Lebih Rendah Dari Tinggi Kerucut");
                }

                if (dTinggiTembereng >= dJariJari){
                    throw new IllegalArgumentException("Tinggi Tembereng Pada Bola Harus Lebih Rendah Daripada Jari-Jari Bola");
                }

                System.out.println("\nLevel 1 :");

                Lingkaran lingkaran = new Lingkaran(dJariJari);
                lingkaran.HitungLingkaran();
                System.out.printf("\n\tLuas Lingkaran : %.3f\n", lingkaran.getdLuas());
                System.out.printf("\tKeliling Lingkaran : %.3f\n", lingkaran.getdKeliling());

                System.out.println("\nLevel 2 :");

                Busur busur = new Busur(dJariJari,dSudut);
                busur.hitungBusur();
                System.out.printf("\tPanjang Busur Tembereng atau Juring 2D : %.3f\n", busur.getBusur());

                Juring juring = new Juring(dJariJari,dSudut);
                juring.hitungJuring();
                System.out.printf("\n\tLuas Juring 2D : %.3f\n", juring.getLuasJuring());
                System.out.printf("\tKeliling Juring 2D : %.3f\n", juring.getKelJuring());

                Tembereng tembereng = new Tembereng(dJariJari,dSudut);
                tembereng.hitungTembereng();
                System.out.printf("\tLuas Tembereng 2D : %.3f\n", tembereng.getLuasTembereng());
                System.out.printf("\tKeliling Tembereng 2D : %.3f\n", tembereng.getKelTembereng());

                Bola bola = new Bola(dJariJari);
                bola.hitung();
                System.out.printf("\n\tVolume Bola : %.3f\n", bola.getVolume());
                System.out.printf("\tLuas Permukaan Bola : %.3f\n", bola.getLuasPermukaan());

                Tabung tabung = new Tabung(dJariJari, dTinggi);
                tabung.hitungSemua();
                System.out.printf("\n\tVolume Tabung : %.3f\n", tabung.getVolume());
                System.out.printf("\tLuas Permukaan Tabung : %.3f\n", tabung.getLuasPermukaan());

                Kerucut kerucut = new Kerucut(dJariJari,dTinggi);
                kerucut.hitung();
                System.out.printf("\n\tVolume Kerucut : %.3f\n", kerucut.getVolume());
                System.out.printf("\tLuas Permukaan Kerucut : %.3f\n", kerucut.getLuasPermukaan());

                System.out.println("\nLevel 3 :");

                Tembereng3D tembereng3D = new Tembereng3D(dJariJari,dTinggi,dTinggiTembereng);
                tembereng3D.processAll();
                System.out.printf("\n\tLuas Permukaan Tembereng 3D : %.3f\n", tembereng3D.getLuasPermukaanTembereng());
                System.out.printf("\tVolume Tembereng 3D : %.3f\n", tembereng3D.getVolumeTembereng());

                Juring3D juring3D = new Juring3D(dJariJari,dTinggi,dTinggiTembereng);
                juring3D.processAll();
                System.out.printf("\tLuas Permukaan Juring 3D : %.3f\n", juring3D.getLuasPermukaanJuring());
                System.out.printf("\tVolume Juring 3D : %.3f\n", juring3D.getVolumeJuring());

                KerucutTerpancung kerucutTerpancung = new KerucutTerpancung(dJariJari,dTinggi,dTinggiTerpancung);
                kerucutTerpancung.hitung();
                System.out.printf("\n\tVolume Kerucut Terpancung : %.3f\n", kerucutTerpancung.getVolume());
                System.out.printf("\tLuas Permukaan Kerucut Terpancung: %.3f\n", kerucutTerpancung.getLuasPermukaan());
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            } finally {
                scanner.nextLine();
                System.out.print("\nMenghitung lagi? ");
                lagi = scanner.nextLine().toLowerCase();
                System.out.println();
                if (lagi.equals("n")){
                    System.out.println("Thank You");
                    exit = true;
                }
            }
        }while(!exit);
    }
}
