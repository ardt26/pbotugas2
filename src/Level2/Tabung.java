package Level2;

import Level1.Lingkaran;

public class Tabung extends Lingkaran {
    private double tinggi;
    private double volume;
    private double luasPermukaan;

    public Tabung(double dJariJari, double tinggi) {
        super(dJariJari);
        this.tinggi = tinggi;
    }

    public void hitungSemua(){
        hitungLuasPermukaan();
        hitungVolume();
    }

    public double hitungVolume(){ // l alas * tinggi
        volume = Lingkaran.dLuas * getTinggi();
        return volume;
    }

    public double hitungLuasPermukaan(){ // 2 pi r (r + t)
        luasPermukaan = (2 * Lingkaran.dLuas) + (super.dKeliling * getTinggi());
        return luasPermukaan;
    }

    public double getVolume(){
        return this.volume;
    }

    public double getLuasPermukaan(){
        return this.luasPermukaan;
    }

    public double getTinggi() {
        return tinggi;
    }
}
