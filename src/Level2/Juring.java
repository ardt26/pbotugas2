package Level2;

public class Juring extends Busur {
    private double kelJuring;
    private double luasJuring;
    private double luasSegitiga;

    public Juring(double dJariJari, double sudut) {
        super(dJariJari, sudut);
    }

    public double hitungKelJuring(){
        this.kelJuring = 2 * super.getdJariJari() + getTaliBusur();
        double kelJuring = this.kelJuring;
        return kelJuring;
    }

    public double hitungLuasJuring(){
        this.luasJuring = (sudut/360) * Math.PI * Math.pow(super.getdJariJari(),2);
        return luasJuring;
    }

    public double hitungLuasSegitiga(){
        this.luasSegitiga = Math.pow(super.getdJariJari(),2) / 2 * Math.sin(radian);
        return luasSegitiga;
    }

    public void hitungJuring(){
        hitungLuasSegitiga();
        hitungLuasJuring();
        hitungKelJuring();
    }

    public double getKelJuring() {
        return kelJuring;
    }

    public double getLuasJuring() {
        return luasJuring;
    }

    public double getLuasSegitiga() {
        return luasSegitiga;
    }
}
