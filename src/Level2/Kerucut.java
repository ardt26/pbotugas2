package Level2;

import Level1.Lingkaran;

public class Kerucut extends Lingkaran {
    private double tinggi;
    private double t_selimut;
    private double volume;
    private double luasPermukaan;
    private double luasSelimutKerucut;

    public Kerucut(double dJariJari, double tinggi) {
        super(dJariJari);
        this.tinggi = tinggi;
    }

    public double hitungVolume() {
        volume = (Lingkaran.dLuas * getTinggi()) / 3;
        return volume;
    }

    public double hitungLuasPermukaan() {
        t_selimut = Math.sqrt(Math.pow(getTinggi(), 2) + Math.pow(getdJariJari(), 2));
        luasSelimutKerucut = (Lingkaran.dLuas * t_selimut) / getdJariJari();
        luasPermukaan = Lingkaran.dLuas + luasSelimutKerucut;
        return luasPermukaan;
    }

    public void hitung(){
        hitungVolume();
        hitungLuasPermukaan();
    }

    public double getVolume() {
        return volume;
    }

    public double getLuasSelimutKerucut() {
        return luasSelimutKerucut;
    }

    public double getLuasPermukaan() {
        return luasPermukaan;
    }

    public double getTinggi() {
        return tinggi;
    }
}
