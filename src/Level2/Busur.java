package Level2;

import Level1.Lingkaran;

public class Busur extends Lingkaran {
    protected double sudut;
    protected double radian = Math.toRadians(sudut);
    private double busur;
    private double taliBusur;

    public Busur (double dJariJari, double sudut){
        super(dJariJari);
        this.sudut = sudut;
    }

    public double hitungBusur(){
        this.busur = (sudut/360) * super.getdKeliling();
        return busur;
    }

    public double hitungTaliBusur(){
        this.taliBusur = 2*super.getdJariJari() * Math.sin(radian/2);
        return taliBusur;
    }

    public double getBusur() { return this.busur; }

    public double getTaliBusur() { return this.taliBusur; }

}
