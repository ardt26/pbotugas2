package Level2;

public class Tembereng extends Juring {
    private double kelTembereng;
    private double luasTembereng;

    public Tembereng(double dJariJari, double sudut) {
        super(dJariJari, sudut);
    }
    public double hitungKelTembereng(){
        this.kelTembereng = getBusur() + super.getTaliBusur();
        return kelTembereng;
    }

    public double hitungLuasTembereng(){
        this.luasTembereng = super.getLuasJuring() - getLuasSegitiga();
        return luasTembereng;
    }

    public void hitungTembereng(){
        hitungLuasTembereng();
        hitungKelTembereng();
    }

    public double getKelTembereng() { return this.kelTembereng; }

    public double getLuasTembereng() { return this.luasTembereng; }
}
