package Level3;
import Level1.Lingkaran;
import Level2.Kerucut;

import java.util.Scanner;

public class KerucutTerpancung extends Kerucut{
    private double selimutDibuang;
    private double luasAlasDibuang;
    private double volumeDibuang;
    private double volume;
    private double jariJariAtas;
    private double luasPermukaanDibuang;
    private double luasPermukaan;
    private double dTinggiTerpancung;

    public KerucutTerpancung(double dJariJari, double tinggi, double dTinggiTepancung) {
        super(dJariJari,tinggi);
        this.dTinggiTerpancung = dTinggiTepancung;
        jariJariAtas = (getdJariJari()*dTinggiTepancung)/tinggi;
        luasAlasDibuang = Math.PI*Math.pow(jariJariAtas,2);
    }

    public double hitungVolume(){
//        this.tinggiDihitung = super.getTinggi() - tinggiDibuang;
//        jariJariAtas = (getdJariJari()*this.tinggiDibuang)/super.getTinggi();
        volumeDibuang = (luasAlasDibuang*dTinggiTerpancung)/3;
        volume = super.hitungVolume() - volumeDibuang;
        return volume;
    }
    public double hitungLuasPermukaan(){
        selimutDibuang = Math.sqrt(Math.pow(dTinggiTerpancung,2)+Math.pow(jariJariAtas,2));
        luasPermukaanDibuang = (Math.PI*selimutDibuang*jariJariAtas); //luasAlasDibuang + (luasAlasDibuang*selimutDibuang)/jariJariAtas;
        luasPermukaan = super.hitungLuasPermukaan() - luasPermukaanDibuang +luasAlasDibuang;  //super.Luas() + (super.Luas()*t_selimut)/getdJariJari();
        return luasPermukaan;
    }

    public void hitung(){
        hitungVolume();
        hitungLuasPermukaan();
    }

    public double getVolume() {
        return volume;
    }

    public double getLuasPermukaan() {
        return luasPermukaan;
    }
}
