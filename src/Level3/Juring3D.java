package Level3;

public class Juring3D extends Tembereng3D {
    private double volumeJuring;
    private double luasPermukaanJuring;

    public Juring3D(double jariJari, double dTinggi, double tinggiTembereng) {
        super(jariJari, dTinggi, tinggiTembereng);

    }

    private double volumeJuringProcess() {
        volumeJuring = super.getVolumeTembereng() + super.hitungVolume();
        return volumeJuring;
    }

    private double luasPermukaanJuringProcess() {
        luasPermukaanJuring = getLuasPermukaanTembereng() + super.getLuasSelimutKerucut();
        return luasPermukaanJuring;
    }

    public double getVolumeJuring() {
        return volumeJuring;
    }

    public double getLuasPermukaanJuring() {
        return luasPermukaanJuring;
    }

    public void processAll() {
        volumeJuringProcess();
        luasPermukaanJuringProcess();
    }
}