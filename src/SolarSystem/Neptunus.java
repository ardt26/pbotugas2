package SolarSystem;

import Level2.Bola;

public class Neptunus extends Bola {
    private double volume;
    private double luasPermukaan;
    private double orbit;
    private double kRotasi;
    private double kRevolusi;

    //jari jari neptunus 24,622 Km
    public Neptunus(double dJariJari, double orbit, double kRotasi, double kRevolusi) {
        super(dJariJari);
        this.orbit = orbit;
        this.kRotasi = kRotasi;
        this.kRevolusi = kRevolusi;
    }

    public double getOrbit() {
        return orbit;
    }

    public double getkRotasi() {
        return kRotasi;
    }

    public double getkRevolusi() {
        return kRevolusi;
    }

    @Override
    public double hitungVolume() {
        volume = super.hitungVolume();
        return volume;
    }

    @Override
    public double hitungLuasPermukaan() {
        luasPermukaan = super.hitungLuasPermukaan();
        return luasPermukaan;
    }

    public double getVolume() {
        return this.volume;
    }

    public double getLuasPermukaan() {
        return this.luasPermukaan;
    }
}
